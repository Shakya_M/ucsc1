#include <stdio.h>
/* This program will obtain an input from the user and output whether that input is a vowel, constant or not in the alphabet */
int main ()
{
	char letter;
	
	printf("Enter any letter\n");
	scanf("%c", &letter);
	
	if(letter>64&&letter<91||letter>96&&letter<123)
	{
		if(letter=='a'|| letter=='e'|| letter=='i'|| letter=='o'|| letter=='u'|| letter=='A'|| letter=='E'|| letter=='I'|| letter=='O'|| letter=='U')
		printf("%c is a vowel letter!\n", letter);
		
		else
		printf("%c is a constant!\n", letter);
		
	}
	else
	printf("It is not in the alphabet\n");
	
	return 0;
}
