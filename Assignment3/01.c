#include <stdio.h>

/* This programme will calculate the sum of given numbers until we enter zero or a negative number */

int main ()
{
	int total=0;
	int num=0;
	
	
	do{
		printf("Enter any number\n");
		scanf("%d", &num);
		if(num<=0)
		break;
		total=total+num;
		
	}
	while(num>0);
	
	printf("Sum of numbers = %d\n", total);
	return 0;

	
	
}
