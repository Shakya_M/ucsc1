#include <stdio.h>
/* This programme will print the multiplication table from 1 to n */

int main()
{
    int i, num;

    
    printf("Enter number to print table: ");
    scanf("%d", &num);

    for(i=1; i<=num; i++)
    {
        printf("%d * %d = %d\n", num, i, (num*i));
    }

    return 0;
}
